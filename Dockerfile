# zakladna image, na ktorej cheme stavat
FROM python:3-slim	
#zlozka, v containery, kde sa bude aplikacia nachadzat 
WORKDIR	/usr/src/app
# instalovanie python packagov
RUN pip install flask	
# kopirovanie aplikacie z prepozitara do containeru
COPY . .	
# s akym prikazom sa container spusti pri docker run
CMD ["python", "app.py"]	

